/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbchelloworld;

import java.sql.*;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class JDBCHelloWorld {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args){
    //Si utilizáis el driver de mysql
    //Class.forName("com.mysql.jdbc.Driver");
    String db = "NBA";
    Connection con;
    Statement query;
    ResultSet result;
    try {
      con = DriverManager.getConnection(
              "jdbc:mysql://localhost/"+db+"?serverTimezone=UTC","alilloig","sql");
      System.out.println("Conexion establecida");
      query = con.createStatement();
      System.out.println("query creada");
      result = query.executeQuery("SELECT * FROM Jugadores");
      System.out.println("query ejecutada");
      System.out.println("Nombre de todos los jugadores de la NBA:");
      while (result.next()){
        System.out.println(
            result.getString("Nombre")
        );
      }
      result.close();
      query.close();
      con.close();
    } catch (SQLException ex) {
      System.out.println("Error en la conexion con "+db+": "+ex.toString());
    }
  }
  
}
